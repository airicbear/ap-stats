{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ***Study Sheet***\n",
    "\n",
    "## **Sampling Distributions**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Meaning of a Sampling Distribution\n",
    "\n",
    "Imagine taking hundreds of samples of size $n$ from a population. If you take the mean of each sample, you'll have a distribution of sample means. If you could somehow take every possible sample from your population, you'd get a distribution of every possible mean. This is called the **sampling distribution** for the statistic.\n",
    "\n",
    "If your original population has mean $\\mu$ and standard deviation $\\sigma$, the sampling distribution of means (also called the distribution of x) will have mean $\\mu_x$ (mu sub-x-bar) and $\\sigma_x$ (sigma sub-x-bar), which are calculated as:\n",
    "\n",
    "$\\mu_\\bar{x} = \\mu$\n",
    "\n",
    "$\\sigma_\\bar{x} = \\frac{\\sigma}{\\sqrt{n}}$\n",
    "\n",
    "Note that the formula for the standard deviation assumes you know $\\sigma$, the population standard deviation. In real life this is unrealistic, and as you continue to study statistics you'll learn what to do when you don't know $\\sigma$.\n",
    "\n",
    "Also note that in a sampling distribution **all** samples are the same size; the sampling distribution of x-bar is the distribution of all x-bars with the **same** sized $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Shape of the Sampling Distribution\n",
    "\n",
    "The shape of the sampling distribution depends both on the sample size and on the shape of the parent population. If the original population is normal, the sampling distribution will also be normal. If the original population is non-normal, then the shape of the sampling distribution depends on the sample size. In these cases, the sampling distribution will have a similar shape to the parent population for small $n$, and become approximately normal for large $n$. In most cases, large is defined as $n > 30$. Outliers or extreme skewness will require larger values of $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Central Limit Theorem\n",
    "\n",
    "The behavior of sampling distributions is summarized by the Central Limit Theorem, which states:\n",
    "\n",
    "1. $\\mu_x = \\mu$, regardeless of the sample size or the shape of the original distribution.\n",
    "\n",
    "2. $\\sigma_\\bar{x} = \\frac{\\sigma}{\\sqrt{n}}$, regardless of the sample size (n) or the shape of the original distribution.\n",
    "\n",
    "3. If the original distribution is normal, the shape of the sampling distribution of the sample mean will be normal for any $n$.\n",
    "\n",
    "4. For small sample sizes, if the original distribution is non-normal, the shape of the sampling distribution of the sample mean will be similar to the shape of the original population. That is, if the original population was skewed to the right, the sampling distribution of the sample mean for small $n$ will also be skewed to the right, although less so than the original population.\n",
    "\n",
    "5. If the sample size is large, the sampling distribution of the sample mean will be approximately normal (many texts define large as $n > 30$, although the actual value will depend on the shape of the original population)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sample Probabilities\n",
    "\n",
    "Since a sampling distribution for large $n$ is normal, you can use normal probabilities to find the probabilities for sample values. For example:\n",
    "\n",
    "A distribution of x has mean 50 and standard deviation 5. What's the probability of drawing a sample with a mean of less than 48?\n",
    "\n",
    "To answer the question, find the z-score for a sample mean of 48, and find the probability for values below that z-score:\n",
    "\n",
    "$z = \\frac{48 - 50}{5} = -0.4$\n",
    "\n",
    "$P(z < -0.4) = 0.3446$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Study Questions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. In your own words, define the term sampling distribution.\n",
    "\n",
    "    - A sampling distribution is a distribution of the means of $N$ samples that often approximates a normal distribution when $N > 30$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. In your own words, describe the five main points of the Central Limit Theorem as defined above.\n",
    "\n",
    "    - The sample mean is the same as the original mean\n",
    "    - The sample standard deviation is the original standard deviation divided by the square root of 2\n",
    "    - If the original distribution is normal, then the sampling distribution of the sample mean is normal\n",
    "    - For small sample sizes, the original distribution's shape will influence the sampling distribution's shape \n",
    "    - For large sample sizes, the sampling distribution's shape will approximate that of a normal distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Jeff is an eggplant inspector for Central Michigan County. Based on his 25 years experience on the job, Jeff assumes the mean weight of the eggplant population is 1,500 grams and the standard deviation is 50 grams.\n",
    "\n",
    "    1. Assuming that Jeff's assumption is correct (it's a debatable assumption, but go with it anyway for now), what are the mean and the standard deviation of the sampling distribution for samples of size $n = 5$?\n",
    "    \n",
    "        - $\\mu_\\overline{x} = 1500$\n",
    "        - $\\sigma_\\overline{x} = 22.361$\n",
    "        \n",
    "    2. Assuming Jeff's assumption is correct, what are the mean and the standard deviation of the sampling distribution for samples of size $n = 30$?\n",
    "    \n",
    "        - $\\mu_\\overline{x} = 1500$\n",
    "        - $\\sigma_\\overline{x} = 9.129$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. We draw samples of size 9 from a distribution that's skewed strongly to the right. The mean of the population distribution is 20 and the standard deviation is 5.\n",
    "\n",
    "    1. What are the mean and the standard deviation of the sampling distribution?\n",
    "    \n",
    "        - $\\mu_\\overline{x} = 20$\n",
    "        - $\\sigma_\\overline{x} = 1.6667$\n",
    "    \n",
    "    2. What's the shape of the sampling distribution?\n",
    "    \n",
    "        - Skewed to the right"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. We draw samples of size 9 from a normal distribution. The mean of the distribution is 10 and the standard deviation is 2.\n",
    "\n",
    "    1. What are the mean and standard deviation of the sampling distribution?\n",
    "    \n",
    "        - $\\mu_\\overline{x} = 10$\n",
    "        - $\\sigma_\\overline{x} = \\frac{2}{3}$\n",
    "        \n",
    "    2. What's the shape of the sampling distribution?\n",
    "    \n",
    "        - Normal\n",
    "        \n",
    "    3. For this population, what's the probability of getting a sample of size 9 with a mean less than 12?\n",
    "    \n",
    "        - $z = \\frac{12 - 10}{2 / 3} = 3$\n",
    "        - $P(z < 3) = 0.9987$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6. We draw samples of size 144 from a distribution that's bi-modal. The mean of the distribution is 60 and the standard deviation is 10.\n",
    "\n",
    "    1. What are the mean and the standard deviation of the sampling distribution?\n",
    "    \n",
    "        - $\\mu_\\overline{x} = 60$\n",
    "        - $\\sigma_\\overline{x} = \\frac{5}{6}$\n",
    "    \n",
    "    2. What's the shape of the sampling distribution?\n",
    "    \n",
    "        - Normal\n",
    "    \n",
    "    3. For this population, what's the probability of getting a sample of size 144 with a mean less than 58?\n",
    "    \n",
    "        - $z = \\frac{58 - 60}{5 / 6} = -2.4$\n",
    "        - $P(z < -2.4) = 0.0082$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "7. As the sample size gets larger, what happens to the standard deviation of the sampling distribution? Why?\n",
    "\n",
    "    - The standard deviation of the sampling distribution gets smaller because of the formula $\\frac{\\sigma}{\\sqrt{n}}$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.1.0",
   "language": "julia",
   "name": "julia-1.1"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.1.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
